﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAM1.MODEL
{
    public class Main
    {
        [JsonProperty("temp")]
        public string Temp { get; set; }

        [JsonProperty("feels_like")]
        public string Feels_like { get; set; }

        [JsonProperty("temp_min")]
        public string Temp_min { get; set; }

        [JsonProperty("temp_max")]
        public string Temp_max { get; set; }

        [JsonProperty("pressure")]
        public string Pressure { get; set; }

        [JsonProperty("humidity")]
        public string Humidity { get; set; }

        public Main()
        {
            this.Temp = string.Empty;
            this.Feels_like = string.Empty;
            this.Temp_min = string.Empty;
            this.Temp_max = string.Empty;
            this.Pressure = string.Empty;
            this.Humidity = string.Empty;
        }
    }
}
