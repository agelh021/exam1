﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAM1.MODEL
{
    public class Coord
    {
        [JsonProperty("lon")]
        public string lon { get; set; }

        [JsonProperty("lat")]
        public string lat { get; set; }

        public Coord()
        {
            this.lon = string.Empty;
            this.lat = string.Empty;
        }
    }
}
