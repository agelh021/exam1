﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAM1.MODEL
{
    public class Weather
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("main")]
        public string Main { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        public Weather()
        {
            this.Id = string.Empty;
            this.Main = string.Empty;
            this.Description = string.Empty;
            this.Icon = string.Empty;
        }
    }
}
