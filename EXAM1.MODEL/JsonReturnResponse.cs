﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAM1.MODEL
{
    public class JsonReturnResponse
    {

        [JsonProperty("coord")]
        public Coord Coord { get; set; }

        [JsonProperty("weather")]
        public List<Weather> Weather { get; set; }

        [JsonProperty("base")]
        public string Base { get; set; }

        [JsonProperty("main")]
        public Main Main { get; set; }

        [JsonProperty("visibility")]
        public string Visibility { get; set; }

        [JsonProperty("wind")]
        public Wind Wind { get; set; }

        [JsonProperty("dt")]
        public string Dt { get; set; }

        [JsonProperty("sys")]
        public Sys Sys { get; set; }

        [JsonProperty("timezone")]
        public string Timezone { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("cod")]
        public string Cod { get; set; }


        public JsonReturnResponse()
        {
            this.Coord = new Coord();
            this.Weather = new List<Weather>();
            this.Base = string.Empty;
            this.Main = new Main();
            this.Visibility = string.Empty;
            this.Wind = new Wind();
            this.Dt = string.Empty;
            this.Sys = new Sys();
            this.Timezone = string.Empty;
            this.Id = string.Empty;
            this.Name = string.Empty;
            this.Cod = string.Empty;

        }
    }
}
