﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAM.API
{
    public class APIHandling
    {
        Requests req = new Requests();
        public string GetRequest(string url, string apiKey)
        { 
            string response = req.GetRequest(url + apiKey);
            return response;
        }
    }
}
