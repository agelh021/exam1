﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAM1.MODEL
{
    public class Wind
    {
        [JsonProperty("speed")]
        public string Speed { get; set; }

        [JsonProperty("deg")]
        public string Deg { get; set; }

        public Wind()
        {
            this.Speed = string.Empty;
            this.Deg = string.Empty;
        }
    }
    
}
