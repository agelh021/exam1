﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAM1.MODEL.XML
{
    public class XMLReturnResponse
    {
  
        public string id { get; set; }
        public string name { get; set; }
        public string lon { get; set; }
        public string lat { get; set; }
        public string rise { get; set; }
        public string set { get; set; }


        public XMLReturnResponse()
        {
           

        }
    }
}
