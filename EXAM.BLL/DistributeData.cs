﻿using EXAM1.MODEL;
using EXAM1.MODEL.XML;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace EXAM.BLL
{
    public class DistributeData
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger("SystemLogger");
        public string PropertyList(object obj)
        {
            var props = obj.GetType().GetProperties();
            var sb = new StringBuilder();
            foreach (var p in props)
            {
                sb.AppendLine(p.Name + ": " + p.GetValue(obj, null));
            }
            return sb.ToString();
        }


        public JsonReturnResponse DeserializeJSonResponse(string jsonString)
        {
            JsonReturnResponse jsonReturn = new JsonReturnResponse();

            try
            {
                logger.Info("DeserializeJSonResponse() -- Deserializing In Progress...");
                jsonReturn = JsonConvert.DeserializeObject<JsonReturnResponse>(jsonString);

            }
            catch (Exception ex)
            {
                logger.Info("DeserializeJSonResponse() -- Exception Encountered :" + ex.Message);
                
            }

            return jsonReturn;
        }

        public string DeserializeXMLObject(string filename)
        {
            string result = string.Empty;
            try
            {

                logger.Info("DeserializeXMLObject() -- Deserializing In Progress...");

                XmlSerializer serializer = new
                XmlSerializer(typeof(XMLReturnResponse));

                XDocument doc;
                using (StringReader s = new StringReader(filename))
                {
                    doc = XDocument.Load(s);
                }
                // A FileStream is needed to read the XML document.
                FileStream fs = new FileStream(doc.ToString(), FileMode.Open);
                XmlReader reader = XmlReader.Create(fs);

                // Declare an object variable of the type to be deserialized.
                XMLReturnResponse i;

                // Use the Deserialize method to restore the object's state.
                i = (XMLReturnResponse)serializer.Deserialize(reader);
                fs.Close();

            }
            catch (Exception ex)
            {
                logger.Info("DeserializeXMLObject() -- Exception Encountered :" + ex.Message);
                result = ex.Message;
            }

            return result;
        }

    }
}
