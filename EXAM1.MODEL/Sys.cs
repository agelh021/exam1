﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAM1.MODEL
{
    public class Sys
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("sunrise")]
        public string Sunrise { get; set; }

        [JsonProperty("sunset")]
        public string Sunset { get; set; }
        public Sys()
        {
            this.Type = string.Empty;
            this.Id = string.Empty;
            this.Country = string.Empty;
            this.Sunrise = string.Empty;
            this.Sunset = string.Empty;
        }

    }
}
