﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EXAM1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h2>Consuming Web Services</h2>
<%--        <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS, and JavaScript.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>--%>
        Input Url: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtboxURL" runat="server" Width="757px">https://api.openweathermap.org/data/2.5/weather?q=London</asp:TextBox>
        <br />
        Input API KEY (appID) <asp:TextBox ID="txtboxappID" runat="server" Width="706px">4e1ca2098a56d09f77b984cf1ad32126</asp:TextBox>
        <br /> 
        <div>
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="138px" OnClick="btnSubmit_Click"/>

        </div>
        <br />
        <div>
        Preview Per Object<br />
        <asp:TextBox ID="txtPreview" runat="server" Height="156px" Width="394px" ReadOnly="True" TextMode="MultiLine"></asp:TextBox>
       </div>
        <br />
        <div>
        Preview RAW Data<br />
        <asp:TextBox ID="txtRawView" runat="server" Height="156px" Width="394px" ReadOnly="True" TextMode="MultiLine"></asp:TextBox>
       </div>
    </div>


</asp:Content>
