﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace EXAM.API
{
    public class Requests
    {
        public const SslProtocols _Tls12 = (SslProtocols)0x00000C00;
        public const SecurityProtocolType Tls12 = (SecurityProtocolType)_Tls12;

        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger("SystemLogger");
        public string GetRequest(string url)
        {
            string result = string.Empty;
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(CheckValidationResult);
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.DefaultConnectionLimit = 9999;
                ServicePointManager.SecurityProtocol = Tls12 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3 | (SecurityProtocolType)768 | (SecurityProtocolType)3072;
               
                HttpWebRequest http = (HttpWebRequest)WebRequest.Create(new Uri(url));
                http.Accept = "application/json";
                http.ContentType = "application/json";
                http.Method = "GET";
               // http.Headers.Add("appid", apikey);
              //  http.Headers.Add("Api-Version", "100");

                var response = http.GetResponse();
                var stream = response.GetResponseStream();
                StreamReader sr = new StreamReader(stream, Encoding.UTF8);
                string content = sr.ReadToEnd();
                result = content;
            }
            catch (Exception ex)
            {
                logger.Error("Get Request Error Result:" + result);
                logger.Error("Exception Message :" + ex.Message);
                logger.Error("InnerException Message :" + ex.InnerException);
                if (string.IsNullOrEmpty(result))
                {
                    logger.Info("Processing Failed: " + url);
                    result = "Error Occured: " + ex.Message + ", While Fetching Data From: " + url;
                }
            }

            return result;
        }
        public static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {   // Always Accept
            return true;
        }
    }
}
