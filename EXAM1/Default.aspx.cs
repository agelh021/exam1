﻿using EXAM.API;
using EXAM.BLL;
using EXAM1.MODEL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EXAM1
{
    public partial class _Default : Page
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger("SystemLogger");
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        private void ConsumeAPI(string url, string apiKey)
        {
            logger.Info("ConsumeApi() -- In Progress...");

            APIHandling apiCall = new APIHandling();
            string requestResult = apiCall.GetRequest(url, apiKey);

            logger.Info("ConsumeApi() -- Received the Result : " + requestResult);
            txtRawView.Text = requestResult;

            DistributeData bll = new DistributeData();

            if (requestResult.StartsWith("<"))
            {
                logger.Info("ConsumeApi() -- Result was Identified as XML");
                string xmlResult = bll.DeserializeXMLObject(requestResult);
                txtPreview.Text = xmlResult;
            }
            else if (requestResult.StartsWith("{") || requestResult.StartsWith("["))
            {
                logger.Info("ConsumeApi() -- Result was Identified as JSON");
                JsonReturnResponse jsonReturn = bll.DeserializeJSonResponse(requestResult);
                txtPreview.Text = bll.PropertyList(jsonReturn);
            }
                    
  
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            ConsumeAPI(txtboxURL.Text, "&appid=" + txtboxappID.Text);
        }

        


    }
}